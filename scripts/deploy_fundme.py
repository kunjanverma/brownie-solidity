import scripts.helpers
from brownie import accounts, config, network, FundMe
import os

def deploy():
    deploy_account = scripts.helpers.get_account(0)

    price_feed_address = scripts.helpers.getPriceFeedContractAddress()

    print ("Deploying FundMe..")
    deployed_contract = FundMe.deploy(
        price_feed_address, 
        {"from": deploy_account}, 
        publish_source=config["networks"][network.show_active()].get("verify")
    )
    print ("FundMe deployed")

def main():
    deploy()
