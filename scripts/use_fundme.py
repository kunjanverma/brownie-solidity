from brownie import FundMe
import scripts.helpers
import scripts.deploy_fundme

def fund(funder_account, value=25000000000000000):
    if len(FundMe) <= 0:
        scripts.deploy_fundme.deploy()

    fund_me = FundMe[-1]
    tx = fund_me.fund({"from": funder_account, "value": value})
    tx.wait(1)

def withdraw(withdraw_address):
    if len(FundMe) <= 0:
        scripts.deploy_fundme.deploy()

    fund_me = FundMe[-1]
    tx = fund_me.withdraw({"from": withdraw_address})
    tx.wait(1)
     
def main():
    funder_account = scripts.helpers.get_account(1)
    fund(funder_account)
    deploy_account = scripts.helpers.get_account(0)
    withdraw(deploy_account)