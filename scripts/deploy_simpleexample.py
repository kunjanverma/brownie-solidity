from brownie import accounts, config, Example1, network
import os
import scripts.helpers


def deploy_simple_example():
    deploy_account = scripts.helpers.get_account(0)
    access_contract_account = scripts.helpers.get_account(1)

    deployed_contract = Example1.deploy({"from": deploy_account})
    print(deployed_contract.get(2))
    transaction = deployed_contract.store(99, {"from": access_contract_account})
    transaction.wait(1)
    print(deployed_contract.get(3))

def main():
    deploy_simple_example()
