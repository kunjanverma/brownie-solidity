from brownie import accounts, config, network, MockV3Aggregator
from web3 import Web3

DECIMALS=18
START_ETH_PRICE=2000
LOCAL_BLOCKCHAIN_ENVIRONMENTS = ["development", "easy-trail"]

def get_account(index):
    if network.show_active() in LOCAL_BLOCKCHAIN_ENVIRONMENTS:
        return accounts[index]
    else:
        return accounts.add(config["wallets"]["private_key_" + str(index)])

def deployOrGetLatest_MockV3Aggregator():
    deploy_account = get_account(0)
    if len(MockV3Aggregator) <= 0:
        print ("Deploying Mocks...")
        MockV3Aggregator.deploy(DECIMALS, Web3.toWei(START_ETH_PRICE, "ether"), {"from":deploy_account})
        print ("Mocks deployed")
    else:
        print ("Using latest deployed MockV3Aggregator at", MockV3Aggregator[-1])
    return MockV3Aggregator[-1]

def getPriceFeedContractAddress():
    if network.show_active() in LOCAL_BLOCKCHAIN_ENVIRONMENTS:
         price_feed_address = deployOrGetLatest_MockV3Aggregator()
    else:
         price_feed_address = config["networks"][network.show_active()]["eth_usd_price_feed_address"]
    
    return price_feed_address