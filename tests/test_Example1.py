from brownie import Example1, accounts, network
from scripts.helpers import LOCAL_BLOCKCHAIN_ENVIRONMENTS
import pytest

def test_deploy():
    if network.show_active() not in LOCAL_BLOCKCHAIN_ENVIRONMENTS:
        pytest.skip("Tests can only run in local blockchain environments")
    # Arrange
    deploy_account = accounts[0]
    # Act
    deployed_contract = Example1.deploy({"from": deploy_account})
    initial_val = deployed_contract.get(2)
    # assert
    assert initial_val == 196


def test_store():
    if network.show_active() not in LOCAL_BLOCKCHAIN_ENVIRONMENTS:
        pytest.skip("Tests can only run in local blockchain environments")
    # Arrange
    deploy_account = accounts[0]
    user_account = accounts[0]
    deployed_contract = Example1.deploy({"from": deploy_account})
    # Act
    transaction = deployed_contract.store(99, {"from": user_account})
    transaction.wait(1)
    expected_val = deployed_contract.get(3)
    # assert
    assert expected_val == 297
