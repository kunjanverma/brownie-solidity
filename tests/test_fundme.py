from brownie import FundMe, network, exceptions
import scripts.use_fundme, pytest
from scripts.helpers import LOCAL_BLOCKCHAIN_ENVIRONMENTS

def test_fund_and_withdraw():
    if network.show_active() not in LOCAL_BLOCKCHAIN_ENVIRONMENTS:
        pytest.skip("Tests can only run in local blockchain environments")

    deploy_account = scripts.helpers.get_account(0)
    access_contract_account = scripts.helpers.get_account(1)
    
    scripts.use_fundme.fund(access_contract_account, value=30000000000000000)
    fund_me_contract=FundMe[-1]
    assert fund_me_contract.addressToAmountFunded(access_contract_account) == 30000000000000000
    scripts.use_fundme.withdraw(deploy_account)
    assert fund_me_contract.addressToAmountFunded(access_contract_account) == 0

def test_only_owner_can_withdraw():
    if network.show_active() not in LOCAL_BLOCKCHAIN_ENVIRONMENTS:
        pytest.skip("Tests can only run in local blockchain environments")

    access_contract_account = scripts.helpers.get_account(1)
    
    scripts.use_fundme.fund(access_contract_account, value=30000000000000000)
    fund_me_contract=FundMe[-1]
    assert fund_me_contract.addressToAmountFunded(access_contract_account) == 30000000000000000

    with pytest.raises(exceptions.VirtualMachineError):
        scripts.use_fundme.withdraw(access_contract_account)